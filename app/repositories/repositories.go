package repositories

import (
	"context"
	"go-arch/app/entities"
)

type Repository interface {
	Fetch(ctx context.Context, num int64) (res []*entities.Article, err error)
}

