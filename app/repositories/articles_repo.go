package repositories

import (
	"context"
	"database/sql"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"go-arch/app/entities"
	conn "go-arch/pkg/mysql"
	"os"
)

const (
	timeFormat = "2006-01-02T15:04:05.999Z07:00" // reduce precision from RFC3339Nano as date format
)

type mysqlArticleRepository struct {
	Conn *sql.DB
}

// NewMysqlArticleRepository will create an object that represent the article.Repository interface
func NewMysqlArticleRepository() Repository {

	dbConn, err := conn.SqlInit()

	if err != nil {
		log.Errorf("Unable to open database %v", err)
		os.Exit(1)	}

	return &mysqlArticleRepository{dbConn}

}

func (m *mysqlArticleRepository) fetch(ctx context.Context, query string, args ...interface{}) ([]*entities.Article, error) {
	rows, err := m.Conn.QueryContext(ctx, query,args...)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	defer func() {
		err := rows.Close()
		if err != nil {
			logrus.Error(err)
		}
	}()

	result := make([]*entities.Article, 0)
	for rows.Next() {
		t := new(entities.Article)
		authorID := int64(0)
		err = rows.Scan(
			&t.ID,
			&t.Title,
			&t.Content,
			&authorID,
			&t.UpdatedAt,
			&t.CreatedAt,
		)

		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		t.Author = entities.Author{
			ID: authorID,
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlArticleRepository) Fetch(ctx context.Context,  num int64) ([]*entities.Article, error) {

	query := `SELECT id,title,content, author_id, updated_at, created_at
  						FROM article ORDER BY created_at LIMIT ?`

	res, err := m.fetch(ctx, query, num)

	if err != nil {
		return nil, err
	}

	return res,  err

}

